[[ -f ~/.bashrc ]] && . ~/.bashrc

[[ $- == *i* ]] && hash zsh >/dev/null 2>&1 && {
    [ -z "$ZSH_VERSION" ] && export SHELL=$(which zsh) && exec $SHELL -l
} || true
