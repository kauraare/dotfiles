;; init
(package-initialize)

(custom-set-variables
 '(package-selected-packages (quote (##)))
 '(xterm-mouse-mode t))

(custom-set-faces)

;; set backup directory
(setq backup-directory-alist '(("." . "~/.emacs.d/backups/"))
      auto-save-file-name-transforms '((".*" "~/.emacs.d/backups/" t))
      )

;; show line numbers
(global-display-line-numbers-mode)

;; native line numbers
(setq-default display-line-numbers 'visual
              display-line-numbers-current-absolute t
              display-line-numbers-width 3 
              display-line-numbers-widen t)
(set-face-attribute 'line-number nil
                    :background "#000000" :foreground "#585858")
(set-face-attribute 'line-number-current-line nil
                    :background "#000000" :foreground "#D8D8D8")

;; scrolling
(global-set-key (kbd "<mouse-4>") 'scroll-down-line)
(global-set-key (kbd "<mouse-5>") 'scroll-up-line)

;; save tabs as spaces
 (defvar untabify-this-buffer)
 (defun untabify-all ()
   "Untabify the current buffer, unless `untabify-this-buffer' is nil."
   (and untabify-this-buffer (untabify (point-min) (point-max))))
 (define-minor-mode untabify-mode
   "Untabify buffer on save." nil " untab" nil
   (make-variable-buffer-local 'untabify-this-buffer)
   (setq untabify-this-buffer (not (derived-mode-p 'makefile-mode)))
   (add-hook 'before-save-hook #'untabify-all))
 (add-hook 'prog-mode-hook 'untabify-mode)
