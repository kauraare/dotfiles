# Source file
source $HOME/.config/antigen/antigen.zsh

# Load the oh-my-zsh's library
antigen use oh-my-zsh

# Load plugins
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle colored-man-pages
antigen bundle colorize
antigen bundle gitignore
antigen bundle pip

# Load the theme
antigen theme dieter

# Tell antigen that you're done
antigen apply

# Fix some things
ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=accept-line-or-clear-warning
SAVEHIST=1000000
HISTSIZE=1000000
zstyle ':completion:*' rehash true

# load aliases
[[ -r $HOME/.alias ]] && source $HOME/.alias

# load some functions
[[ -r $HOME/main/tracks/scripts/functions.sh ]] && source $HOME/main/tracks/scripts/functions.sh


# Find uninstalled commands from official repositories using pkgfile
[[ -r /usr/share/doc/pkgfile/command-not-found.zsh ]] && source /usr/share/doc/pkgfile/command-not-found.zsh
true
