#!/bin/bash

cd $(dirname "$0")

set -eux

mnt="$1"
hostname="$2"
kernel=linux
rootpasswd=$(sudo -u kaur bash -c "source /home/kaur/.profile && pass devices/archjunn/root")
kaurpasswd=$(sudo -u kaur bash -c "source /home/kaur/.profile && pass devices/archjunn/kaur")

# Basic system
yes '' | pacstrap -i "$mnt" "${pkgs[@]}" $(cat packages/{0-general,2-networking}) $kernel
(head -n1 /etc/fstab; genfstab -U "$mnt" | grep -v '^#') | column -t > "$mnt/etc/fstab"
echo "$hostname" >"$mnt/etc/hostname"
echo "127.0.0.1 localhost" >> "$mnt/etc/hosts"
echo "::1 localhost"       >> "$mnt/etc/hosts"

# Add mirrorlist
reflector --latest 32 --protocol https --sort rate > "$mnt/etc/pacman.d/mirrorlist"

# Configure network and access
rsync -a /etc/vconsole.conf "$mnt/etc"
rsync -a /etc/netctl "$mnt/etc/"

ln -sf ../run/systemd/resolve/stub-resolv.conf "$mnt/etc/resolv.conf"

# Initramfs configuration and boot loader entry

sed -i '/^HOOKS/s/\(filesystems\)/consolefont \1/' "$mnt/etc/mkinitcpio.conf"
install -Dm644 /dev/stdin "$mnt/boot/loader/entries/arch-${kernel}.conf" <<- EOF
    title   Arch ($kernel)
    linux   /vmlinuz-$kernel
    initrd  /intel-ucode.img
    initrd  /initramfs-$kernel.img
    options root=$(cat $mnt/etc/fstab | grep ' / ' | cut -f1 -d' ') rw
EOF

# Create and run init script
install -Dm755 /dev/stdin "$mnt/root/init.sh" <<- EOF
    #!/bin/bash
    set -eux
    # Locale
    echo "LANG=en_GB.UTF-8" >/etc/locale.conf
    sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
    sed -i 's/#\(en_GB\.UTF-8\)/\1/' /etc/locale.gen
    locale-gen
    # Timezone
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime
    # Generate initramfs and install bootloader
    mkinitcpio -P
    bootctl install
    # Enable systemd services
    systemctl enable sshd.service
    systemctl set-default multi-user.target
    # Users and accounts
    useradd -s /usr/bin/zsh -m kaur
    groupadd -r sudo
    gpasswd -a kaur sudo
    cp /etc/sudoers /etc/sudoers.new
    echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.new
    visudo -cf /etc/sudoers.new
    mv /etc/sudoers.new /etc/sudoers
    echo $rootpasswd | tee /dev/stderr | sed -E 's/^(.*)$/\1\n\1/' | passwd
    echo $kaurpasswd | tee /dev/stderr | sed -E 's/^(.*)$/\1\n\1/' | passwd kaur
    # initialize dotfiles repository
    sudo -u kaur bash -c '
        cd /home/kaur
        rm -rf ..?* .[!.]* *
        git clone https://gitlab.com/kauraare/dotfiles.git .
        git submodule update --init
    '
EOF

arch-chroot "$mnt" /root/init.sh
