# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# load aliases
[[ -r $HOME/.alias ]] && source $HOME/.alias
