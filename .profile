export PASSWORD_STORE_DIR=$HOME/main/password-store
export PASSWORD_STORE_GIT=$HOME/main/password-store
export EDITOR='emacs -nw'
export BROWSER='/usr/bin/chromium'
export BROWSERNEWWINDOW="$BROWSER --new-window"
export SYSTEMD_LESS=FRSMK
export LESS=FRX
export PATH=~/.local/bin:~/.venv/bin:~/.bin:$PATH
export FPATH="${FPATH}:$HOME/.bin/completions"
export GRB_LICENSE_FILE=$HOME/main/keys/$(hostname).gurobi.lic 
export TEXMFHOME=$HOME/.texmf
export JQ_COLORS="0;31:0;39:0;39:0;39:0;32:1;39:1;39"
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"

[ -n "$TMUX" ]                                              && return
command -v startx      > /dev/null && [ "$XDG_VTNR" = "1" ] && exec startx
command -v mytmux tmux > /dev/null                          && exec mytmux
